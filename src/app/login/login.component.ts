import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { LoginService } from '../service/login.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    submitted = false;
    listArr: any = [];
  loginForm: FormGroup;
    loginService: any;
  
    constructor( private loginservice:LoginService,private router: Router) { }

    ngOnInit() {
        this.loginForm = new FormGroup({
            "email": new FormControl(''),
            "password": new FormControl(''),
        })
    }

    onLogin() {
        console.log("Registration form :", this.loginForm.value)
        this.submitted = true;
        this.loginservice
            .onLogin(this.loginForm.value)
            .subscribe(data => {
                console.log("data : ", data)
                localStorage.setItem("Angular5Demo-Token", data);
                // this.router.navigate(['/dashboard']);
                //  return false;
                localStorage.setItem("role",data.data.role);
                if(localStorage.getItem("role")== "admin"){
                this.router.navigate(['/dashboard']);
  
                }
                else if(localStorage.getItem("role") == "user"){
                    this.router.navigate(['/error'])
                }
            })
    }
}


    
