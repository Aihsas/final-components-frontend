import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DeleteuserService {

    constructor(private http:HttpClient) { }
    deleteuser(id):Observable<any>{
      console.log("login form data : ", id)
      return this.http.delete('http://localhost:3000/deleteitem?id='+id)
    
      //localhost:27017/market
  }
  
  }
  