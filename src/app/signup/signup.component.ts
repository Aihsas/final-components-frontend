import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { SignupService } from '../service/signup.service';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss'],
    animations: [routerTransition()]
})
export class SignupComponent implements OnInit {
    registerForm: FormGroup;
    submitted = false;
    listArr: any = [];
    constructor( private signupService:SignupService) { }

    ngOnInit() {
        this.registerForm = new FormGroup({
            "name": new FormControl(''),
            "email": new FormControl(''),
            "password": new FormControl(''),
        })

        // this.registerForm = this.formBuilder.group({
        //     firstName: ['', Validators.required],
        //     email: ['', [Validators.required]],
        //     password: ['', [Validators.required, Validators.minLength(6)]]
        // });
    }

    // convenience getter for easy access to form fields
    //get f() { return this.registerForm.controls; }

    onSubmit() {
        console.log("Registration form :", this.registerForm.value)
        this.submitted = true;

        // stop here if form is invalid

        this.signupService
            .onSubmit(this.registerForm.value)
            .subscribe(data => {

            })
        // console.log(this.registerForm.value)
    }



}


