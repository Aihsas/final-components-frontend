import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HeaderComponent } from './components/header/header.component';
//import { ListitemComponent } from './listitem/listitem.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListitemComponent } from './listitem/listitem.component';
import { ListuserComponent } from './listuser/listuser.component';

@NgModule({
    imports: [FormsModule,ReactiveFormsModule,
        CommonModule,
        LayoutRoutingModule,
        TranslateModule,
        NgbDropdownModule.forRoot()
    ],
    declarations: [LayoutComponent, SidebarComponent, HeaderComponent, ListitemComponent, ListuserComponent]
})
export class LayoutModule {}
