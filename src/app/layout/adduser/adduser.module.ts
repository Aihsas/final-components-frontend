import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdduserRoutingModule } from './adduser-routing.module';
import { AdduserComponent } from './adduser.component';
import { ReactiveFormsModule } from '@angular/forms'
import { FormsModule, FormGroup }   from '@angular/forms'
@NgModule({
  imports: [
    CommonModule,AdduserRoutingModule,ReactiveFormsModule,FormsModule,
 
  ],
  declarations: [AdduserComponent]
})
export class AdduserModule { }
