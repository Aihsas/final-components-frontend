import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { AdduserService } from '../../service/adduser.service';

@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.scss']
})
export class AdduserComponent implements OnInit {
  submitted = false;
  listArr: any = [];
  adduser: FormGroup;
  constructor(private adduserservice:AdduserService) {}
    
  ngOnInit() {
    this.adduser = new FormGroup({
      "name": new FormControl(''),
      "email": new FormControl(''),
      "password": new FormControl(''),
      "role": new FormControl(''),
  })
}
addUsers(){
  console.log("Registration form :", this.adduser.value)
  this.submitted = true;

  // stop here if form is invalid

  this.adduserservice
      .addUsers(this.adduser.value)
      .subscribe(data => {

      })
  // console.log(this.registerForm.value)
}
}









