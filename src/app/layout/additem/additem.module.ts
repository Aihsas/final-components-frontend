import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdditemRoutingModule } from './additem-routing.module';
import { AdditemComponent } from './additem.component';
import { ReactiveFormsModule } from '@angular/forms'
import { FormsModule, FormGroup }   from '@angular/forms'
@NgModule({
  imports: [
    CommonModule,AdditemRoutingModule,ReactiveFormsModule,FormsModule,
 
  ],
  declarations: [AdditemComponent]
})
export class AdditemModule { }
