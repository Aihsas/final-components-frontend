import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { AdditemService } from '../../service/additem.service';

@Component({
  selector: 'app-additem',
  templateUrl: './additem.component.html',
  styleUrls: ['./additem.component.scss']
})
export class AdditemComponent implements OnInit {
  addproduct: FormGroup;
  submitted = false;
  listArr: any = [];
  constructor(private additemService:AdditemService) { }

  ngOnInit() {
    this.addproduct = new FormGroup({
      "pname": new FormControl(''),
      "pdescription": new FormControl(''),
      "pcost": new FormControl(''),
      "status": new FormControl(''),
})
}
addProducts() {
  console.log("Registration form :", this.addproduct.value)
  this.submitted = true;

  // stop here if form is invalid

  this.additemService
      .addProducts(this.addproduct.value)
      .subscribe(data => {

      })
  // console.log(this.registerForm.value)
}



}






  

