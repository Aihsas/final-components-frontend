import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';
import { SignupComponent } from './signup/signup.component';
import { HomeComponent } from './home/home.component';
//import { DashboardComponent } from './layout/dashboard/dashboard.component';

const routes: Routes = [
    { path: '', component:SignupComponent },
    { path: 'login', loadChildren: './login/login.module#LoginModule' },
    { path: 'signup', loadChildren: './signup/signup.module#SignupModule' },
    { path:'layout', loadChildren:'./layout/layout.module#LayoutModule'},
    { path:'dashboard', loadChildren:'./layout/layout.module#LayoutModule'},
    { path: 'error', loadChildren: './server-error/server-error.module#ServerErrorModule' },
    { path: 'access-denied', loadChildren: './access-denied/access-denied.module#AccessDeniedModule' },
    { path: 'tables', loadChildren: './layout/tables/tables.module#TablesModule'},
    { path: 'charts', loadChildren: './layout/charts/charts.module#ChartsModule'},
    { path: 'forms', loadChildren: './layout/charts/charts.module#ChartsModule'},



    // { path: 'not-found', loadChildren: './not-found/not-found.module#NotFoundModule' },
    // { path: '**', redirectTo: 'not-found' },
  
   
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
